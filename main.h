#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <errno.h>
#include <unistd.h>

#define COMMAND_MODE 0
#define SCRIPT_MODE 1

void execute(char** tokens);
