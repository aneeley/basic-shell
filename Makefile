shell: main.o
	c99 -lreadline main.o -o shell

main.o: main.c main.h
	c99 -c main.c main.h

clean:
	rm shell *.o *.gch
