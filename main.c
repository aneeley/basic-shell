#include "main.h"

int main(int argc, char* argv[]) {

    char* cmds[] = { "cd", "exit" };
    size_t num_cmds = sizeof(cmds) / sizeof(char*);

    char* tokens[64];
    char* token;
    char* line;

    int mode;
    FILE* fp;

    signal(SIGINT, SIG_IGN);

    switch (argc) {
        case 1:
            mode = COMMAND_MODE;
            break;
        default:
            mode = SCRIPT_MODE;
            fp = fopen(argv[1], "r");
            line = malloc(256);
            break;
    }

    while (1) {

        // get line

        char* ptr;

        switch (mode) {
            case COMMAND_MODE:
                line = readline("% ");
                break;
            case SCRIPT_MODE:
                if (!feof(fp)) {
                    fgets(line, 256, fp);
                } else {
                    fclose(fp);
                    exit(0);
                }
                break;
        }

        if ((ptr = strchr(line, '#')) != NULL)
            *ptr = '\0';

        // toke it up

        int count = 0;

        token = strtok(line, " \t\n\r");

        while (token != NULL) {
            tokens[count++] = token;
            token = strtok(NULL, " \t\n\r");
        }

        tokens[count] = NULL;

        if (!tokens[0])
            continue;

        // cmds

        for (count = 0; count < num_cmds; count++)
            if (strcmp(tokens[0], cmds[count]) == 0)
                break;

        switch (count) {
            case 0:
                chdir(tokens[1]);
                break;
            case 1:
                exit(0);
                break;
            default:
                execute(tokens);
                break;
        }
    }
    return 0;
}

void execute(char** tokens) {
    pid_t pid = fork();

    // child

    if (pid == 0) {
        if (execvp(tokens[0], tokens) == -1) {
            fprintf(stderr, "exec failed: %s\n", strerror(errno));
            exit(1);
        }
    }

    // parent

    if (pid > 0) {
        int status;
        pid_t child_pid = wait(&status);
        int exit_status = status >> 8;
    }

    // error

    if (pid < 0) {
        fprintf(stderr, "can't fork: %s\n", strerror(errno));
        exit(1);
    }
}

